# go-gpt3
[![GoDoc](http://img.shields.io/badge/GoDoc-Reference-blue.svg)](https://godoc.org/gitlab.com/aknudsen/go-gpt3)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/aknudsen/go-gpt3)](https://goreportcard.com/report/gitlab.com/aknudsen/go-gpt3)


[OpenAI GPT-3](https://beta.openai.com/) Go API

Installation:
```
go get gitlab.com/aknudsen/go-gpt3
```


Example usage:

```go
package main

import (
	"context"
	"fmt"
	gogpt "gitlab.com/aknudsen/go-gpt3"
)

func main() {
	c := gogpt.NewClient("your token")
	ctx := context.Background()

	req := gogpt.CompletionRequest{
		Model: "ada",
		MaxTokens: 5,
		Prompt:    "Lorem ipsum",
	}
	resp, err := c.CreateCompletion(ctx, req)
	if err != nil {
		return
	}
	fmt.Println(resp.Choices[0].Text)
}
```
